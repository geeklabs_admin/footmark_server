package com.geeklabs.footmark.controller;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.footmark.dto.UserRegisterDto;
import com.geeklabs.footmark.service.UserService;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	private UserService userService;
	
	@RequestMapping(value = "/signin", method = RequestMethod.POST)
	@ResponseBody
	public UserRegisterDto regiser(@RequestBody UserRegisterDto userDto) throws IOException {
		return userService.registerUser(userDto);
	}

	@RequestMapping(value = "/signout/{userId}", method = RequestMethod.GET)
	public @ResponseBody UserRegisterDto signOut(@PathVariable long userId) {
		return userService.updateSigninStatus(userId);
	}
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
