package com.geeklabs.footmark.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.footmark.domain.Track;
import com.geeklabs.footmark.dto.AdvancedSearchDto;
import com.geeklabs.footmark.dto.TrackDto;
import com.geeklabs.footmark.pagination.Pageable;
import com.geeklabs.footmark.response.json.ResponseStatus;
import com.geeklabs.footmark.service.TrackService;

@Controller
@RequestMapping(value = "/track")
public class TrackController {

	private TrackService trackService;

	// save tracks
	@RequestMapping(value = "/saveTracks/{userId}", method = RequestMethod.POST)
	@ResponseBody
	public List<TrackDto> saveListOfTrack(@RequestBody List<TrackDto> trackDtos, @PathVariable long userId) {
		
		try {
			trackDtos  = trackService.saveTracks(trackDtos, userId);
		} catch (IOException e) {
			// TODO
			e.printStackTrace();
		}
		return trackDtos;
		
	}

	//get defaultTracks
	@RequestMapping(value = "/defaultTracks/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public List<TrackDto> getRecentTracks(@PathVariable long userId,
			@RequestParam("currentPage") int currentPage) {

		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage);
		return trackService.getRecentTracks(userId, pageable);
	}

	//get userTracks
	@RequestMapping(value = "/userTracks/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public List<TrackDto> getUserTracks(@PathVariable long userId,
			@RequestParam("currentPage") int currentPage) {

		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage);
		return trackService.getUserCapturedTracks(userId, pageable);
	}
	
	//set favouriteTracks
	@RequestMapping(value = "/favouriteTracks", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus saveFavouriteTracks(@RequestBody TrackDto trackDto) {
		ResponseStatus responseStatus = new ResponseStatus();
		trackService.saveTrackAsFavourite(trackDto);
		//responseStatus.setStatus("Error");
		responseStatus.setStatus("success");
		return responseStatus;

	}
	
	@RequestMapping(value = "/getFavorite/{userId}", method = RequestMethod.GET)
	@ResponseBody
	public List<TrackDto> getFavoriteTracks(@PathVariable long userId, @RequestParam("currentPage") int currentPage) {
		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage);
		return trackService.getFavoriteResults(userId, pageable);
	}

	// delete track
	@RequestMapping(value = "/delete/{trackId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseStatus delete(@PathVariable long trackId) {
		trackService.deleteTracks(trackId);
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@RequestMapping(value = "/deleteMultipleTracks", method = RequestMethod.DELETE)
	@ResponseBody
	public ResponseStatus deleteSelectedTracks(@RequestBody List<Track> tracks) {
		trackService.deleteSelectedTracks(tracks);
		ResponseStatus responseStatus = new ResponseStatus();
		responseStatus.setStatus("success");
		return responseStatus;
	}

	@RequestMapping(value = "/byDate/{userId}", method = RequestMethod.GET)
	public List<TrackDto> getTracksByDate(
			@RequestBody AdvancedSearchDto advancedSearchDto,
			@PathVariable long userId) {
		Pageable pageable = new Pageable();
		pageable.setOffset(0);
		return trackService.getUserTracks(userId, advancedSearchDto, pageable);

	}
	
	@RequestMapping(value = "/search/{userId}")
	@ResponseBody
	public List<TrackDto> genericSearch( @PathVariable long userId, 
			@RequestParam("currentPage") int currentPage, @RequestParam("searchQuery") String searchQuery) {
		
		Pageable pageable = new Pageable();
		pageable.setOffset(currentPage);
		
		List<TrackDto> searchResults = trackService.getSearchResults(userId,pageable,searchQuery);
		return searchResults;
	}

	public void setTrackService(TrackService trackService) {
		this.trackService = trackService;
	}
}
