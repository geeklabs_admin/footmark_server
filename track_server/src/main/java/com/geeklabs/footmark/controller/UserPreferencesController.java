package com.geeklabs.footmark.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.footmark.dto.UserPreferencesDto;
import com.geeklabs.footmark.service.UserPreferencesService;

@Controller
@RequestMapping(value = "/userpreferences")
public class UserPreferencesController {

	private UserPreferencesService userPreferencesService;

	@RequestMapping(value = "/save/{userId}", method = RequestMethod.POST)
	public @ResponseBody UserPreferencesDto save(@RequestBody UserPreferencesDto userPreferencesDto, @PathVariable long userId) {
		return userPreferencesService.save(userPreferencesDto, userId);
	}

	public void setUserPreferencesService(UserPreferencesService userPreferencesService) {
		this.userPreferencesService = userPreferencesService;
	}
}
