package com.geeklabs.footmark.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.geeklabs.footmark.dto.FirstTrackDto;
import com.geeklabs.footmark.response.json.ResponseStatus;
import com.geeklabs.footmark.service.FirstTrackService;

@Controller
@RequestMapping(value = "/firstTrack")
public class FirstTrackController {

	@Autowired
	private FirstTrackService firstTrackService;
	
	@RequestMapping(value = "/save/{userId}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseStatus saveFirstTrack(@RequestBody FirstTrackDto firstTrackDto, @PathVariable String userId) {
		return firstTrackService.saveFirstTrack(firstTrackDto, userId);
	}
}
