package com.geeklabs.footmark.service;

import com.geeklabs.footmark.domain.Country;
import com.geeklabs.footmark.dto.CountryDto;

public interface CountryService {
	public Country getCountry(CountryDto countryDto);

}
