package com.geeklabs.footmark.service;

import com.geeklabs.footmark.dto.UserPreferencesDto;

public interface UserPreferencesService {
	
	public UserPreferencesDto save(UserPreferencesDto userPreferencesDto,long userId);

}
