package com.geeklabs.footmark.service.impl;

import java.io.IOException;

import com.geeklabs.footmark.domain.Document;
import com.geeklabs.footmark.service.BlobService;
import com.geeklabs.footmark.service.DocumentService;

public class DocumentServiceImpl implements DocumentService {

	private BlobService blobService;
	
	@Override
	public Document getDocument(String identifier) {
		try {
			return blobService.getFile(identifier);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setBlobService(BlobService blobService) {
		this.blobService = blobService;
	}
}
