package com.geeklabs.footmark.service.impl;

import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.dozer.DozerBeanMapper;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.footmark.domain.State;
import com.geeklabs.footmark.dto.StateDto;
import com.geeklabs.footmark.repository.StateRepository;
import com.geeklabs.footmark.service.StateService;

public class StateServiceImpl implements StateService {

	private StateRepository stateRepository;
	private DozerBeanMapper dozerBeanMapper;
	
	private HashMap<String, State> stateList = new HashMap<String, State>();

	public State getStateByCode(StateDto stateDto) {
		String stateCode = stateDto.getCode();
		State state = stateList.get(stateCode);
		return state;
	}

	@Override
	@Transactional
	public State getState(StateDto stateDto) {
		State state = getStateByCode(stateDto);
		if (state == null) {
			state = dozerBeanMapper.map(stateDto, State.class);
			stateRepository.save(state);
			stateList.put(state.getCode(), state);
		}
		return state;
	}

	@PostConstruct
	public void init() {
		Iterable<State> listOfStates = stateRepository.findAll();
		Iterator<State> iterator = listOfStates.iterator();
		while (iterator.hasNext()) {
			State state = iterator.next();
			stateList.put(state.getCode(), state);
		}
	}
	
	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}
	
	public void setStateRepository(StateRepository stateRepository) {
		this.stateRepository = stateRepository;
	}

}
