package com.geeklabs.footmark.service;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.geeklabs.footmark.domain.Track;
import com.geeklabs.footmark.dto.AdvancedSearchDto;
import com.geeklabs.footmark.dto.TrackDto;
import com.geeklabs.footmark.pagination.Pageable;

public interface TrackService {

	public List<TrackDto> saveTracks(List<TrackDto> trackDtos, long userId)
			throws ClientProtocolException, IOException;

	List<TrackDto> getRecentTracks(long userId, Pageable pageable);

	public void deleteTracks(long trackId);

	public void deleteSelectedTracks(List<Track> trackIds);

	public List<TrackDto> getUserTracks(long userId,
			AdvancedSearchDto advancedSearchDto, Pageable pageable);

	public List<TrackDto> getUserCapturedTracks(long userId, Pageable pageable);

	public void saveTrackAsFavourite(TrackDto trackDto);

	public List<TrackDto> getSearchResults(long userId, Pageable pageable,
			String searchQuery);

	public List<TrackDto> getFavoriteResults(long userId, Pageable pageable);
}
