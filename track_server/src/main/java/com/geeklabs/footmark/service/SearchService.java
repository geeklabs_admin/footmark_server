package com.geeklabs.footmark.service;

import java.sql.Timestamp;
import java.util.List;

import com.geeklabs.footmark.dto.TrackDto;

public interface SearchService {

	List<TrackDto> getListOfTracks(long userId, Timestamp fromDate, Timestamp toDate);

}
