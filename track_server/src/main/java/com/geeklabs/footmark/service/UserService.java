package com.geeklabs.footmark.service;

import java.io.IOException;

import com.geeklabs.footmark.domain.User;
import com.geeklabs.footmark.dto.UserRegisterDto;

public interface UserService {
	 UserRegisterDto updateSigninStatus(long id);
	 UserRegisterDto registerUser(UserRegisterDto userDto) throws IOException;
	 User getUserByUserId(long userId);
	 boolean validateUUID(String uuid, long userId);
}
