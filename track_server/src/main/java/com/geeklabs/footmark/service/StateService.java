package com.geeklabs.footmark.service;

import com.geeklabs.footmark.domain.State;
import com.geeklabs.footmark.dto.StateDto;

public interface StateService {

	public State getState(StateDto stateDto);
}
