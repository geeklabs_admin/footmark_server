package com.geeklabs.footmark.service;

import com.geeklabs.footmark.domain.Document;

public interface DocumentService {
	Document getDocument(String identifier);
}
