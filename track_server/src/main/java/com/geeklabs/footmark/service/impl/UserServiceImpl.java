package com.geeklabs.footmark.service.impl;

import java.io.IOException;

import org.dozer.DozerBeanMapper;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.footmark.domain.User;
import com.geeklabs.footmark.domain.UserPreferences;
import com.geeklabs.footmark.dto.UserPreferencesDto;
import com.geeklabs.footmark.dto.UserRegisterDto;
import com.geeklabs.footmark.facade.UserFacade;
import com.geeklabs.footmark.repository.UserPreferencesRepository;
import com.geeklabs.footmark.repository.UserRepository;
import com.geeklabs.footmark.service.EmailService;
import com.geeklabs.footmark.service.UserService;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson.JacksonFactory;
import com.google.api.services.oauth2.Oauth2;
import com.google.api.services.oauth2.model.Tokeninfo;

public class UserServiceImpl implements UserService {

	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport();
	private static final JacksonFactory FACTORY = new JacksonFactory();

	private UserRepository userRepository;
	private UserPreferencesRepository userPreferencesRepository;
	private UserFacade userFacade;
	private DozerBeanMapper dozerBeanMapper;

	@Override
	@Transactional
	public UserRegisterDto updateSigninStatus(long id) {
		User user = userRepository.findOne(id);
		user.setUserUuid(null);
		userRepository.save(user);
		UserRegisterDto userRegisterDto = new UserRegisterDto();
		userRegisterDto.setStatus("success");
		return userRegisterDto;
	}

	@Override
	@Transactional
	public UserRegisterDto registerUser(UserRegisterDto userDto)
			throws IOException {
		Tokeninfo tokeninfo = getUserTokenInfo(userDto.getAccessToken());
		if (!tokeninfo.containsKey("error")) {
			// Is user exist
			User user = userRepository.getUserByEmail(tokeninfo.getEmail());

			// Create user
			if (user == null) { 
				user = userFacade.convertToUserDomain(tokeninfo, userDto);
				EmailService emailService = new EmailServiceImpl();
				emailService.send(user.getEmail(),
						"Footmark Registration",
						"Hi, \n \n Thank you for installing Footmark app."
						+ "This app captures your daily moments and it tracks Your Location by default, "
						+ "so that you can recall your past at any time, anywhere. \n \n "
								+ " Thanks \n" + " Footmark Team");
				userDto.setStatus("register");
			} else { // check whether user uuid active or Inactive
				String resignIn = userDto.getResignIn();
				String userUuid = userDto.getUserUuid();
				if (resignIn.equals("true")) {
					EmailService emailService = new EmailServiceImpl();
					emailService.send(user.getEmail(), // FIXME -> Fix this email message.
						"Footmark resign-in from other device",
						"Hi, \n \nYour tracking started from other device:  "
						+ "\n \nthe device details are \n\n"
						+"Device Model:"+userDto.getModel()+"\n\n"
						+"Device API:"+userDto.getSdk()+"\n\n"
								+ "If u want to continue with previous device please uninstall and install the app"
								+ " Thanks \n" + " Footmark Team");
					
					// Update user domain
					user.setUserUuid(userUuid);
					userDto.setStatus("success");
				} else {
					String uuid = user.getUserUuid();
					// Trying to sign in, but uuid exist in db, i.e. trying to re-signin from other device
					if (uuid != null && !uuid.equals(userUuid)) {
						userDto.setStatus("UuidInActive");
						return userDto;
					} else { // normal signin
						user.setUserUuid(userUuid);
						userDto.setStatus("success");
					}
				}
				
				// set UserPreferences
				UserPreferences userPreferences = userPreferencesRepository
						.getUserPreferencesByUserId(user.getId());
				if (userPreferences != null) {
					UserPreferencesDto userPreferencesDto = dozerBeanMapper
							.map(userPreferences, UserPreferencesDto.class);
					userDto.setPreferencesDto(userPreferencesDto);
				}
			}
			userRepository.save(user);
			userDto.setUserId(user.getId());
		}
		return userDto;
	}

	private Tokeninfo getUserTokenInfo(String token) throws IOException {

		GoogleCredential credential = new GoogleCredential().setAccessToken(token);
		Oauth2 oauth2 = new Oauth2.Builder(HTTP_TRANSPORT, FACTORY, credential)
				.build();
		Tokeninfo tokeninfo = oauth2.tokeninfo().setAccessToken(token)
				.execute();
		return tokeninfo;
	}

	@Override
	public User getUserByUserId(long userId) {
		User user = userRepository.findOne(userId);
		return user;
	}

	@Override
	@Transactional(readOnly = true)
	public boolean validateUUID(String uuid, long userId) {
		return userRepository.isUUIDExist(uuid, userId);
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public void setUserFacade(UserFacade userFacade) {
		this.userFacade = userFacade;
	}

	public void setUserPreferencesRepository(
			UserPreferencesRepository userPreferencesRepository) {
		this.userPreferencesRepository = userPreferencesRepository;
	}

	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}
}