package com.geeklabs.footmark.service.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.util.Random;

import com.geeklabs.footmark.domain.Document;
import com.geeklabs.footmark.service.BlobService;
import com.google.appengine.api.appidentity.AppIdentityService;
import com.google.appengine.api.appidentity.AppIdentityServiceFactory;
import com.google.appengine.tools.cloudstorage.GcsFileMetadata;
import com.google.appengine.tools.cloudstorage.GcsFileOptions;
import com.google.appengine.tools.cloudstorage.GcsFilename;
import com.google.appengine.tools.cloudstorage.GcsInputChannel;
import com.google.appengine.tools.cloudstorage.GcsOutputChannel;
import com.google.appengine.tools.cloudstorage.GcsService;
import com.google.appengine.tools.cloudstorage.GcsServiceFactory;
import com.google.appengine.tools.cloudstorage.RetryParams;

public class BlobServiceImpl implements BlobService {

	// private static final int BUFFER_SIZE = 3 * 1024 * 1024;
	AppIdentityService appIdentity = AppIdentityServiceFactory.getAppIdentityService();
	String bucketName = appIdentity.getDefaultGcsBucketName();

	/**
	 * This is where backoff parameters are configured. Here it is aggressively
	 * retrying with backoff, up to 10 times but taking no more that 15 seconds
	 * total to do so.
	 */

	private final GcsService gcsService = GcsServiceFactory.createGcsService(new RetryParams.Builder().initialRetryDelayMillis(10).retryMaxAttempts(10)
			.totalRetryPeriodMillis(15000).build());

	@Override
	public String storeBlob(String originalFileName, byte[] capturedImage, String prefix) throws IOException {
		GcsOutputChannel outputChannel;
		GcsFileOptions.Builder builder = new GcsFileOptions.Builder();
		// builder.addUserMetadata(key, value)
		// builder.mimeType(mimeType);
		String fileName = prefix + "_" + new Random().nextInt() + "_" + originalFileName.replace(".", "_");

		GcsFilename gcsFilename = getFileName(fileName);
		outputChannel = gcsService.createOrReplace(gcsFilename, builder.build());
		// Copy content to Cloud Storage
		copy(capturedImage, Channels.newOutputStream(outputChannel));

		return fileName;
	}

	@Override
	public Document getFile(String fileName) throws IOException {
		GcsFilename gcsFileName = getFileName(fileName);

		GcsFileMetadata metadata = gcsService.getMetadata(gcsFileName);
		if (metadata != null) {
			Document document = new Document();
			document.setMimeType(metadata.getOptions().getMimeType());
			document.setFileName(metadata.getFilename().getObjectName());

			int fileSize = (int) metadata.getLength();
			ByteBuffer result = ByteBuffer.allocate(fileSize);
			try (GcsInputChannel readChannel = gcsService.openReadChannel(gcsFileName, 0)) {
				readChannel.read(result);
			}
			document.setContent(result.array());
			return document;
		}
		return null;
	}

	private GcsFilename getFileName(String fileName) {
		return new GcsFilename("geeklabsfm.appspot.com", fileName); // niceskilldev.appspot.com
	}

	/**
	 * Transfer the data from the inputStream to the outputStream. Then close
	 * both streams.
	 */
	private void copy(byte[] content, OutputStream output) throws IOException {
		try {
			output.write(content);
		} finally {
			output.close();
		}
	}

	@Override
	public boolean deleteFile(String fileName) throws IOException {
		if (fileName != null && !fileName.isEmpty()) {
			GcsFilename gcsFileName = getFileName(fileName);
			boolean isDeleted = gcsService.delete(gcsFileName);
			return isDeleted;
		}
		
		return false;
	}
}
