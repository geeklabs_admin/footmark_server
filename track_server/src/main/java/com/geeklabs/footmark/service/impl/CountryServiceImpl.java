package com.geeklabs.footmark.service.impl;

import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.dozer.DozerBeanMapper;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.footmark.domain.Country;
import com.geeklabs.footmark.dto.CountryDto;
import com.geeklabs.footmark.repository.CountryRepository;
import com.geeklabs.footmark.service.CountryService;

public class CountryServiceImpl implements CountryService {

	private CountryRepository countryRepository;
	private DozerBeanMapper dozerBeanMapper;
	
	private HashMap<String, Country> countryList = new HashMap<String, Country>();
	
	@Override
	@Transactional
	public Country getCountry(CountryDto countryDto) {
		Country country = getCountryByCode(countryDto);
		if (country == null) {
			country = dozerBeanMapper.map(countryDto, Country.class);
			  countryRepository.save(country);
			  countryList.put(countryDto.getCode(), country);
		}
		return country;
	}

	public Country getCountryByCode(CountryDto countryDto) {
		String countryCode = countryDto.getCode();
		Country country = countryList.get(countryCode);
		return country;
	}
	
	@PostConstruct
	public void init(){
		Iterable<Country> listOfCountrys = countryRepository.findAll();
		Iterator<Country> iterator = listOfCountrys.iterator();
		while (iterator.hasNext()) {
			Country country = iterator.next();
			countryList.put(country.getCode(),country);
		}
	}

	public void setCountryRepository(CountryRepository countryRepository) {
		this.countryRepository = countryRepository;
	}
	
	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}
}
