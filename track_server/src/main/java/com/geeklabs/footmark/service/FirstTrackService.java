package com.geeklabs.footmark.service;

import com.geeklabs.footmark.dto.FirstTrackDto;
import com.geeklabs.footmark.response.json.ResponseStatus;

public interface FirstTrackService {

	ResponseStatus saveFirstTrack(FirstTrackDto firstTrackDto, String userId);

}
