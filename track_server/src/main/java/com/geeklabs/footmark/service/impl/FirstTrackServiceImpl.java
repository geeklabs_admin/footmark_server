package com.geeklabs.footmark.service.impl;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.footmark.domain.FirstTrack;
import com.geeklabs.footmark.dto.FirstTrackDto;
import com.geeklabs.footmark.repository.FirstTrackRepository;
import com.geeklabs.footmark.response.json.ResponseStatus;
import com.geeklabs.footmark.service.FirstTrackService;

@Service
public class FirstTrackServiceImpl implements FirstTrackService {

	@Autowired
	private FirstTrackRepository firstTrackRepository;
	
	@Autowired
	private DozerBeanMapper dozerBeanMapper;
	
	@Override
	@Transactional
	public ResponseStatus saveFirstTrack(FirstTrackDto firstTrackDto, String userId) {
		ResponseStatus responseStatus = new ResponseStatus();
		FirstTrack firstTrack = dozerBeanMapper.map(firstTrackDto, FirstTrack.class);
		firstTrackRepository.save(firstTrack);
		responseStatus.setStatus("success");
		return responseStatus;
	}
}

