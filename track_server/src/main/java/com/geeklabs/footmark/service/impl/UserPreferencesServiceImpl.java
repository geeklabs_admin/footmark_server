package com.geeklabs.footmark.service.impl;

import org.dozer.DozerBeanMapper;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.footmark.domain.User;
import com.geeklabs.footmark.domain.UserPreferences;
import com.geeklabs.footmark.dto.UserPreferencesDto;
import com.geeklabs.footmark.repository.UserPreferencesRepository;
import com.geeklabs.footmark.service.UserPreferencesService;
import com.geeklabs.footmark.service.UserService;

public class UserPreferencesServiceImpl implements UserPreferencesService {

	private UserPreferencesRepository userPreferencesRepository;
	private DozerBeanMapper dozerBeanMapper;
	private UserService userService;
	
	@Override
	@Transactional
	public UserPreferencesDto save(UserPreferencesDto userPreferencesDto, long userId) {
		UserPreferences userPreferences = new UserPreferences();
		
		if (userPreferencesDto.getId() > 0) {
			userPreferences = userPreferencesRepository.findOne(userPreferencesDto.getId());
		}

		userPreferences.setDistance(userPreferencesDto.getDistance());
		userPreferences.setTime(userPreferencesDto.getTime());
		User userByUserId = userService.getUserByUserId(userId);
		userPreferences.setUser(User.key(userByUserId.getId()));
		
		userPreferencesRepository.save(userPreferences);
		
		userPreferencesDto.setStatus("success");
		userPreferencesDto.setId(userPreferences.getId());
		return userPreferencesDto;
	}
	
	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}
	
	public void setUserPreferencesRepository(
			UserPreferencesRepository userPreferencesRepository) {
		this.userPreferencesRepository = userPreferencesRepository;
	}
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public DozerBeanMapper getDozerBeanMapper() {
		return dozerBeanMapper;
	}
}
