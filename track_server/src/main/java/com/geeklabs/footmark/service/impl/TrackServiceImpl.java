package com.geeklabs.footmark.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.footmark.domain.Track;
import com.geeklabs.footmark.domain.User;
import com.geeklabs.footmark.dto.AdvancedSearchDto;
import com.geeklabs.footmark.dto.TrackDto;
import com.geeklabs.footmark.facade.TrackFacade;
import com.geeklabs.footmark.lucene.impl.IndexStatus;
import com.geeklabs.footmark.lucene.impl.LuceneManager;
import com.geeklabs.footmark.pagination.Pageable;
import com.geeklabs.footmark.repository.TrackRepository;
import com.geeklabs.footmark.service.BlobService;
import com.geeklabs.footmark.service.TrackService;
import com.geeklabs.footmark.service.UserService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class TrackServiceImpl implements TrackService {

	private TrackRepository trackRepository;
	private TrackFacade trackFacade;
	private UserService userService;
	private BlobService blobService;

	@Override
	@Transactional
	public List<TrackDto> getRecentTracks(long userId, Pageable pageable) {
		List<Track> tracks = trackRepository.getTracksDefaultTracks(userId,
				pageable);
		return trackFacade.convertTrackDomainToTrackDto(tracks);
	}

	@Override
	@Transactional
	public List<TrackDto> getUserCapturedTracks(long userId, Pageable pageable) {
		List<Track> userOwnTracks = trackRepository.getUserOwnTracks(userId,
				pageable);
		return trackFacade.convertTrackDomainToTrackDto(userOwnTracks);
	}

	@Override
	@Transactional
	public List<TrackDto> getUserTracks(long userId,
			AdvancedSearchDto searchDto, Pageable pageable) {
		List<Track> tracksByDate = trackRepository.getTracksByDate(userId,
				searchDto, pageable);
		trackFacade.convertTrackDomainToTrackDto(tracksByDate);
		return null;
	}

	@Override
	@Transactional
	public List<TrackDto> saveTracks(List<TrackDto> trackDtos, long userId)
			throws ClientProtocolException, IOException {
		Map<Track, IndexStatus> tracksToPersist = Maps.newHashMap();
		
		List<Track> newTrackList = new ArrayList<Track>();
		List<TrackDto> newTrackDtoList = new ArrayList<TrackDto>();
		
		for (TrackDto trackDto : trackDtos) {
			Track trackDomain = null;
			if (trackDto.getServerTrackId() != null) { // Update track
				trackDomain = trackRepository.findOne(trackDto.getServerTrackId());
	
				//is track deleted
				if (trackDto.isDeleted()) {
					deleteTracks(trackDomain.getServerTrackId());
				}
				
				//is track as favourite
				if (trackDto.isFavorite()) {
					trackDomain.setFavorite(trackDto.isFavorite());
				}
				
				trackDomain.setContent(trackDto.getContent());
				trackDomain.setTag(trackDto.getTag());
				
				// Delete image if exist 
				String capturedImagePath = trackDomain.getCapturedImagePath(); 
				if (capturedImagePath != null && !capturedImagePath.isEmpty()) {
					blobService.deleteFile(capturedImagePath);
				}
				
				// Save new image 
				if (trackDto.getCapturedImage() != null && trackDto.getCapturedImage().length > 0) {
					saveImage(userId, trackDto);
				}
				trackDomain.setCapturedImageName(trackDto.getCapturedImageName());
				trackDomain.setCapturedImagePath(trackDto.getCapturedImagePath());
				
				tracksToPersist.put(trackDomain, IndexStatus.UPDATE);
				// Persist moments
				trackRepository.save(trackDomain);
			} else { // Save track
				
				//check track already exist or not by date
				trackDomain = isTrackExist(trackDto.getTrackTime(), userId);
				if (trackDomain == null) {
					trackDomain = trackFacade.convertTrackDtoToTrackDomain(trackDto);
					
					//set address to track dto
					trackDto.setAddress(trackDomain.getAddress());
					
					User user = userService.getUserByUserId(userId);
					trackDomain.setUser(User.key(user.getId()));
					
					if (trackDto.getCapturedImage() != null && trackDto.getCapturedImage().length > 0) {
						//store image here
						saveImage(userId, trackDto);
						trackDomain.setCapturedImageName(trackDto.getCapturedImageName());
						trackDomain.setCapturedImagePath(trackDto.getCapturedImagePath());
					}
					tracksToPersist.put(trackDomain, IndexStatus.ADD);
				}
				
				// Persist moments
				trackRepository.save(trackDomain);
				
				newTrackList.add(trackDomain);
			}
		}
		//convert new track list into new trackDto list
		newTrackDtoList = trackFacade.convertTrackDomainToTrackDto(newTrackList);
		return newTrackDtoList;
	}

	private void saveImage(long userId, TrackDto trackDto) {
		String trackPrefix = "" + userId;
		try {
			String capturedImagePath = blobService.storeBlob(trackPrefix, trackDto.getCapturedImage(), trackDto.getCapturedImageName());
			trackDto.setCapturedImagePath(capturedImagePath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Track isTrackExist(Date trackTime, Long userId) {
		return trackRepository.isTrackExist(trackTime, userId);
	}

	@Override
	@Transactional
	public void saveTrackAsFavourite(TrackDto trackDto) {
		Track track = trackRepository.findOne(trackDto.getServerTrackId());
		track.setFavorite(trackDto.isFavorite());
		trackRepository.save(track);
	}

	@Override
	@Transactional
	public List<TrackDto> getFavoriteResults(long userId, Pageable pageable) {
		List<Track> favoriteTracksByUserId = trackRepository
				.getFavoriteTracksByUserId(userId, pageable);
		return trackFacade.convertTrackDomainToTrackDto(favoriteTracksByUserId);
	}

	@Override
	@Transactional
	public List<TrackDto> getSearchResults(long userId, Pageable pageable,
			String searchQuery) {
		try {
			// Get search results from lucene
			List<Long> searchResults = LuceneManager.search(searchQuery);
			
			// Get actual results from data store
			List<Track> tracks = Lists.newArrayList();
			for (Long trackId : searchResults) {
				tracks.add(trackRepository.findOne(trackId));
			}
			return trackFacade.convertTrackDomainToTrackDto(tracks);
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}

		return new ArrayList<>();
	}

	@Override
	@Transactional
	public void deleteTracks(long trackId) {
		trackRepository.delete(trackId);
	}

	@Override
	@Transactional
	public void deleteSelectedTracks(List<Track> tracks) {
		trackRepository.delete(tracks);
	}

	public void setTrackFacade(TrackFacade trackFacade) {
		this.trackFacade = trackFacade;
	}

	public void setTrackRepository(TrackRepository trackRepository) {
		this.trackRepository = trackRepository;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public void setBlobService(BlobService blobService) {
		this.blobService = blobService;
	}
}