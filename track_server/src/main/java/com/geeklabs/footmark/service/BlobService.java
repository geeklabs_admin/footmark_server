package com.geeklabs.footmark.service;

import java.io.IOException;

import com.geeklabs.footmark.domain.Document;


public interface BlobService {

	String storeBlob(String fileName, byte[] capturedImage,  String prefix) throws IOException;
	
	Document getFile(String fileName) throws IOException;
	
	boolean deleteFile(String fileName) throws IOException;
}
