package com.geeklabs.footmark.service.impl;

import java.util.HashMap;
import java.util.Iterator;

import javax.annotation.PostConstruct;

import org.dozer.DozerBeanMapper;
import org.springframework.transaction.annotation.Transactional;

import com.geeklabs.footmark.domain.City;
import com.geeklabs.footmark.dto.CityDto;
import com.geeklabs.footmark.repository.CityRepository;
import com.geeklabs.footmark.service.CityService;

public class CityServiceImpl implements CityService {

	private CityRepository cityRepository;
	private DozerBeanMapper dozerBeanMapper;

	private HashMap<String, City> cityList = new HashMap<String, City>();
	
	@Override
	@Transactional
	public City getCity(CityDto cityDto) {
		City city = getCityByCode(cityDto);
		if (city == null) {
			// save city in db
			 city = dozerBeanMapper.map(cityDto, City.class);
			cityRepository.save(city);
			cityList.put(city.getCode(), city);
		}
		return city;
	}

	public City getCityByCode(CityDto cityDto) {
		String cityCode = cityDto.getCode();
		City city = cityList.get(cityCode);
		return city;
	}

	@PostConstruct
	public void init() {
		Iterable<City> listOfCities = cityRepository.findAll();
		Iterator<City> iterator = listOfCities.iterator();
		while (iterator.hasNext()) {
			City city = iterator.next();
			cityList.put(city.getCode(), city);
		}
	}
	
	public void setCityRepository(CityRepository cityRepository) {
		this.cityRepository = cityRepository;
	}
	
	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}
}


