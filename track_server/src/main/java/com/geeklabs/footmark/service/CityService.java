package com.geeklabs.footmark.service;

import com.geeklabs.footmark.domain.City;
import com.geeklabs.footmark.dto.CityDto;

public interface CityService {
	public City getCity(CityDto cityDto);
}
