package com.geeklabs.footmark.reversegeocoder.model;

import java.util.ArrayList;
import java.util.List;

public class GeoCodeResponse {

	private String status;
	private List<Results> results = new ArrayList<Results>();

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public void setResults(List<Results> results) {
		this.results = results;
	}

	public List<Results> getResults() {
		return results;
	}
}
