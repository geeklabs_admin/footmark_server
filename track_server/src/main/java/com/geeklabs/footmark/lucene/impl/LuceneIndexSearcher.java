package com.geeklabs.footmark.lucene.impl;

import com.geeklabs.footmark.lucene.IndexSearcher;

public class LuceneIndexSearcher implements IndexSearcher {
	public static final LuceneIndexSearcher READER = new LuceneIndexSearcher();
	private org.apache.lucene.search.IndexSearcher searcher;
	
	private LuceneIndexSearcher() {
		searcher = new org.apache.lucene.search.IndexSearcher(LuceneDirectoryReader.READER.reader());
	}
	
	@Override
	public org.apache.lucene.search.IndexSearcher searcher() {
		return searcher;
	}

}
