package com.geeklabs.footmark.lucene;

public interface DirectoryReader {

	org.apache.lucene.index.DirectoryReader reader();
}
