package com.geeklabs.footmark.lucene.impl;

import org.apache.lucene.index.IndexWriterConfig;

import com.geeklabs.footmark.lucene.LuceneIndexWriterConfig;
import com.svenjacobs.lugaene.GaeIndexWriterConfigHelper;

public class LuceneGaeIndexWriterConfig implements LuceneIndexWriterConfig {
	public static final LuceneGaeIndexWriterConfig WRITER_CONFIG = new LuceneGaeIndexWriterConfig();
	private final IndexWriterConfig indexWriterConfig;
	
	private LuceneGaeIndexWriterConfig() {
		indexWriterConfig = GaeIndexWriterConfigHelper.create(LuceneManager.LUCENE_VERSION, LuceneStantardAnalyzer.ANALYZER.analyzer());
	}
	
	@Override
	public IndexWriterConfig writerConfig() {
		return indexWriterConfig;
	}
	
}
