package com.geeklabs.footmark.lucene;

import org.apache.lucene.analysis.Analyzer;

public interface LuceneAnalyzer {

	Analyzer analyzer();
}
