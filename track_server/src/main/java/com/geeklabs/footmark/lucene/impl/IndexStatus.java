package com.geeklabs.footmark.lucene.impl;

public enum IndexStatus {
	ADD, UPDATE, DELETE
}
