package com.geeklabs.footmark.lucene;

public interface IndexSearcher {

	org.apache.lucene.search.IndexSearcher searcher();
}
