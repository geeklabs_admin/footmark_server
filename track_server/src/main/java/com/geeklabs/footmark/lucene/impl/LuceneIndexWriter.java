package com.geeklabs.footmark.lucene.impl;

import java.io.IOException;

import org.apache.lucene.index.IndexWriter;
import com.geeklabs.footmark.lucene.DocumentWriter;

public final class LuceneIndexWriter implements DocumentWriter {
	public static final LuceneIndexWriter INDEX_WRITER = new LuceneIndexWriter();
	private IndexWriter writer;
	
	private LuceneIndexWriter() {
		try {
			writer = new IndexWriter(LuceneGaeDirectory.DIRECTORY.directory(), LuceneGaeIndexWriterConfig.WRITER_CONFIG.writerConfig());
		} catch (IOException e) {
			// TODO - FIXME
			e.printStackTrace();
		}
	}
	
	@Override
	public IndexWriter writer() {
		return writer;
	}
}
