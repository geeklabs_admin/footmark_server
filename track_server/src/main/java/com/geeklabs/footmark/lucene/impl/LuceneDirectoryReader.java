package com.geeklabs.footmark.lucene.impl;

import java.io.IOException;

import com.geeklabs.footmark.lucene.DirectoryReader;

public class LuceneDirectoryReader implements DirectoryReader {
	public static final LuceneDirectoryReader READER = new LuceneDirectoryReader();
	private org.apache.lucene.index.DirectoryReader reader;
	
	private LuceneDirectoryReader() {
		try {
			reader =  org.apache.lucene.index.DirectoryReader.open(LuceneGaeDirectory.DIRECTORY.directory());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public org.apache.lucene.index.DirectoryReader reader() {
		return reader;
	}

}
