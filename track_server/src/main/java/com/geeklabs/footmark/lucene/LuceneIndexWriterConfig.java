package com.geeklabs.footmark.lucene;

import org.apache.lucene.index.IndexWriterConfig;

public interface LuceneIndexWriterConfig {

	IndexWriterConfig writerConfig();
}
