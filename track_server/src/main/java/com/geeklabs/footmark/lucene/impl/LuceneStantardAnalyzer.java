package com.geeklabs.footmark.lucene.impl;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import com.geeklabs.footmark.lucene.LuceneAnalyzer;

public final class LuceneStantardAnalyzer implements LuceneAnalyzer {

	
	public static final LuceneStantardAnalyzer ANALYZER = new LuceneStantardAnalyzer();
	private final Analyzer analyzer;
	
	private LuceneStantardAnalyzer() {
		analyzer = new StandardAnalyzer(LuceneManager.LUCENE_VERSION);
	}
	
	@Override
	public Analyzer analyzer() {
		return analyzer;
	}
}
