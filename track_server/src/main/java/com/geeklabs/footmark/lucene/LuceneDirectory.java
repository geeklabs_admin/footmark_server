package com.geeklabs.footmark.lucene;

import org.apache.lucene.store.Directory;

public interface LuceneDirectory {

	Directory directory();
}
