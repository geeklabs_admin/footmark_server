package com.geeklabs.footmark.lucene.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.util.Version;

import com.geeklabs.footmark.domain.Track;

public final class LuceneManager {

	public static final Version LUCENE_VERSION = Version.LUCENE_44;
	
	private static final String TRACK_ID = "id";
	public static final String ADDRESS = "address";
	public static final String TAG = "tag";
	public static final String CONTENT = "content";

	private LuceneManager() {} 
	
	public static void index(Track track, IndexStatus indexStatus) {
		final IndexWriter indexWriter = LuceneIndexWriter.INDEX_WRITER.writer();
		//changed by balu && indexWriter != null
		if (IndexStatus.ADD == indexStatus && indexWriter != null) {
			addDoc(indexWriter, track);
			//changed by balu && indexWriter != null
		} else if (IndexStatus.UPDATE == indexStatus && indexWriter != null) {
			deleteDoc(indexWriter, track);
			addDoc(indexWriter, track);
			//changed by balu && indexWriter != null
		} else if (IndexStatus.DELETE == indexStatus && indexWriter != null) {
			deleteDoc(indexWriter, track);
		}
	}

	private static void deleteDoc(final IndexWriter indexWriter, Track track) {
		Term trackIdTerm = new Term(TRACK_ID, String.valueOf(track.getServerTrackId())); // uid is primary 
		try {
			indexWriter.deleteDocuments(trackIdTerm);
			indexWriter.commit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static List<Long> search(String query) throws IOException, ParseException {
		// Prepare query object
		QueryParser queryParser = new QueryParser(LUCENE_VERSION, CONTENT, LuceneStantardAnalyzer.ANALYZER.analyzer());
		Query q = queryParser.parse(query);
	
		// Initialize 
		final DirectoryReader reader = DirectoryReader.open(LuceneGaeDirectory.DIRECTORY.directory());
		final IndexSearcher searcher = new IndexSearcher(reader);
		
		int hitsPerPage = 10;
		// Search
		TopScoreDocCollector collector = TopScoreDocCollector.create(hitsPerPage, true);
		searcher.search(q, collector);
		ScoreDoc[] scoreDocs = collector.topDocs().scoreDocs;

		// Prepare docs from search result
		List<Long> trackIds = new ArrayList<>();
		for (int i = 0; i < scoreDocs.length; ++i) {
			Document d = searcher.doc(scoreDocs[i].doc);
			String id = d.get(TRACK_ID);
			if (id != null) {
				trackIds.add(Long.parseLong(id));
			}
		}
		return trackIds;

	}
	
	private static void addDoc(IndexWriter w, Track track) {
		Document doc = new Document();
		Long serverTrackId = track.getServerTrackId();
		if (serverTrackId != null) {
			doc.add(new Field(TRACK_ID, serverTrackId.toString(), unTokenizedType()));
		}
		
		
	    String content = track.getContent();
	    if (content != null) {
	    	doc.add(new Field(CONTENT, content, unStoredType()));
	    }
	    
	    String tag = track.getTag();
	    if (tag != null) {
	    	doc.add(new Field(TAG, tag, unStoredType()));
	    }
	    
	    String address = track.getAddress();
	    if (address != null) {
			doc.add(new Field(ADDRESS, address, unStoredType()));
		}
	    
	    // Think about what if index not happened for specific track ?
	    try {
			w.addDocument(doc);
			w.commit();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static FieldType unTokenizedType() {
		FieldType idType = new FieldType();
		idType.setIndexed(true);
		idType.setStored(true);
		idType.setTokenized(false);
		return idType;
	}

	/**
	 * Indexed and tokenized , but field not stored.
	 * @return
	 */
	private static FieldType unStoredType() {
		FieldType titleType = new FieldType();
		titleType.setIndexed(true);
		return titleType;
	}
}
