package com.geeklabs.footmark.lucene;

import org.apache.lucene.index.IndexWriter;

public interface DocumentWriter {

	IndexWriter writer();
}
