package com.geeklabs.footmark.lucene.impl;

import org.apache.lucene.store.Directory;

import com.geeklabs.footmark.lucene.LuceneDirectory;
import com.svenjacobs.lugaene.GaeDirectory;

public class LuceneGaeDirectory implements LuceneDirectory {
	public static final LuceneGaeDirectory DIRECTORY = new LuceneGaeDirectory();
	private final Directory dir;
	
	private LuceneGaeDirectory() {
		dir = new GaeDirectory("MomentIndex");
	}
	
	@Override
	public Directory directory() {
		return dir;
	}
}
