package com.geeklabs.footmark.dto;

import java.util.Date;

public class TrackDto {
	
	private int id;
	private Long serverTrackId;
	private double latitude;
	private double longitude;
	private Date trackTime;
	private String address;
	private String content;
	private boolean isFavorite;
	private String tag;
	private boolean isDeleted;
	private byte[] capturedImage;
	private String capturedImagePath;
	private String capturedImageName;
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	
	public Date getTrackTime() {
		return trackTime;
	}
	
	public void setTrackTime(Date trackTime) {
		this.trackTime = trackTime;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	public String getContent() {
		return content;
	}
	
	public void setContent(String content) {
		this.content = content;
	}
	

	public Long getServerTrackId() {
		return serverTrackId;
	}
	
	public void setServerTrackId(Long serverTrackId) {
		this.serverTrackId = serverTrackId;
	}
	
	public boolean isFavorite() {
		return isFavorite;
	}
	
	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public String getTag() {
		return tag;
	}
	
	public boolean isDeleted() {
		return isDeleted;
	}
	
	public void setDeleted(boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	public byte[] getCapturedImage() {
		return capturedImage;
	}
	
	public void setCapturedImage(byte[] capturedImage) {
		this.capturedImage = capturedImage;
	}
	
	public String getCapturedImagePath() {
		return capturedImagePath;
	}
	
	public void setCapturedImagePath(String capturedImagePath) {
		this.capturedImagePath = capturedImagePath;
	}
	
	public String getCapturedImageName() {
		return capturedImageName;
	}
	
	public void setCapturedImageName(String capturedImageName) {
		this.capturedImageName = capturedImageName;
	}
}
