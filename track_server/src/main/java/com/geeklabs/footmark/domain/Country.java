package com.geeklabs.footmark.domain;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;

@Entity
public class Country {
	
	public static Key<Country> key(long id) {
	  return Key.create(Country.class, id);
	}
	
	@Id
	private Long id;

	private String name;

	private String code;

	public Long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
