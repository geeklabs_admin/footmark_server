package com.geeklabs.footmark.domain;

import java.util.Date;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

@Entity
public class Track {

	@Id
	private Long serverTrackId;

	private double latitude;
	private double longitude;

	@Index
	private Date trackTime;
	
	@Index
	private String address;

	@Index
	private Key<User> user;
	private Key<Country> country;
	private Key<State> state;
	private Key<City> city;
	
	@Index
	private String content;
	
	@Index
	private boolean isFavorite;
	
	@Index
	private String tag;
	
	@Index
	private String capturedImagePath;
	
	@Index
	private String capturedImageName;

	public Long getServerTrackId() {
		return serverTrackId;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Date getTrackTime() {
		return trackTime;
	}

	public void setTrackTime(Date trackTime) {
		this.trackTime = trackTime;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Key<User> getUser() {
		return user;
	}

	public void setUser(Key<User> user) {
		this.user = user;
	}

	public Key<Country> getCountry() {
		return country;
	}

	public void setCountry(Key<Country> country) {
		this.country = country;
	}

	public Key<State> getState() {
		return state;
	}

	public void setState(Key<State> state) {
		this.state = state;
	}

	public Key<City> getCity() {
		return city;
	}

	public void setCity(Key<City> city) {
		this.city = city;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	public boolean isFavorite() {
		return isFavorite;
	}
	
	public void setFavorite(boolean isFavorite) {
		this.isFavorite = isFavorite;
	}
	
	public String getTag() {
		return tag;
	}
	
	public void setTag(String tag) {
		this.tag = tag;
	}
	

	public String getCapturedImagePath() {
		return capturedImagePath;
	}

	public void setCapturedImagePath(String capturedImagePath) {
		this.capturedImagePath = capturedImagePath;
	}

	public String getCapturedImageName() {
		return capturedImageName;
	}

	public void setCapturedImageName(String capturedImageName) {
		this.capturedImageName = capturedImageName;
	}

	public void setServerTrackId(Long serverTrackId) {
		this.serverTrackId = serverTrackId;
	}
}
