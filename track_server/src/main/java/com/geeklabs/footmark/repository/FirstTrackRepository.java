package com.geeklabs.footmark.repository;

import com.geeklabs.footmark.domain.FirstTrack;
import com.geeklabs.footmark.repository.objectify.ObjectifyCRUDRepository;

public interface FirstTrackRepository extends ObjectifyCRUDRepository<FirstTrack> {

}
