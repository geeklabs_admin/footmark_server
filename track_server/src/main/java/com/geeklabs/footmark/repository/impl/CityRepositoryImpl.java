package com.geeklabs.footmark.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.footmark.domain.City;
import com.geeklabs.footmark.repository.CityRepository;
import com.geeklabs.footmark.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class CityRepositoryImpl extends AbstractObjectifyCRUDRepository<City> implements CityRepository {

	@Autowired
	private Objectify objectify;

	public CityRepositoryImpl() {
		super(City.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
}
