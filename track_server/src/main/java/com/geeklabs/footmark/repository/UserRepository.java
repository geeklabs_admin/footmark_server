package com.geeklabs.footmark.repository;

import com.geeklabs.footmark.domain.User;
import com.geeklabs.footmark.repository.objectify.ObjectifyCRUDRepository;

public interface UserRepository extends ObjectifyCRUDRepository<User> {
	
	public User getUserByEmail(String userEmail);

	boolean isUUIDExist(String uuid, long userId);
}
