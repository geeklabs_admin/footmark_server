package com.geeklabs.footmark.repository;

import com.geeklabs.footmark.domain.State;
import com.geeklabs.footmark.repository.objectify.ObjectifyCRUDRepository;

public interface StateRepository extends ObjectifyCRUDRepository<State> {

}