package com.geeklabs.footmark.repository;

import java.sql.Timestamp;
import java.util.List;

import com.geeklabs.footmark.domain.Track;


public interface SearchCustomRepository {
	List<Track> getTrackListByDate(long userId, Timestamp fromDate, Timestamp toDate);
}
