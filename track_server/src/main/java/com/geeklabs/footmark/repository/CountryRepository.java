package com.geeklabs.footmark.repository;

import com.geeklabs.footmark.domain.Country;
import com.geeklabs.footmark.repository.objectify.ObjectifyCRUDRepository;

public interface CountryRepository extends ObjectifyCRUDRepository<Country> {

}
