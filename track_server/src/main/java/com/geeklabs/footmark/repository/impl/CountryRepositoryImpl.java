package com.geeklabs.footmark.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.footmark.domain.Country;
import com.geeklabs.footmark.repository.CountryRepository;
import com.geeklabs.footmark.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class CountryRepositoryImpl extends AbstractObjectifyCRUDRepository<Country> implements CountryRepository {

	@Autowired
	private Objectify objectify;

	public CountryRepositoryImpl() {
		super(Country.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
}
