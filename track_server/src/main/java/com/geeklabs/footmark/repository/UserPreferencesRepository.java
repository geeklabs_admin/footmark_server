package com.geeklabs.footmark.repository;

import com.geeklabs.footmark.domain.UserPreferences;
import com.geeklabs.footmark.repository.objectify.ObjectifyCRUDRepository;

public interface UserPreferencesRepository extends ObjectifyCRUDRepository<UserPreferences> {

	UserPreferences getUserPreferencesByUserId(Long userId);

}
