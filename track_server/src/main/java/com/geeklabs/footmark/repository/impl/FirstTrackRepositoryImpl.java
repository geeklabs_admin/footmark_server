package com.geeklabs.footmark.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.footmark.domain.FirstTrack;
import com.geeklabs.footmark.repository.FirstTrackRepository;
import com.geeklabs.footmark.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

@Repository
public class FirstTrackRepositoryImpl extends AbstractObjectifyCRUDRepository<FirstTrack> implements FirstTrackRepository{

	@Autowired
	private Objectify objectify;
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}

	public FirstTrackRepositoryImpl() {
		super(FirstTrack.class);
	}
}

