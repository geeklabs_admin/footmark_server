package com.geeklabs.footmark.repository;

import java.util.List;

import com.geeklabs.footmark.domain.Track;

public interface TrackCustomRepository {

	Track editTracksByTrackId(long trackId);
	List<Track>getTracksByUserId(long userId);
}
