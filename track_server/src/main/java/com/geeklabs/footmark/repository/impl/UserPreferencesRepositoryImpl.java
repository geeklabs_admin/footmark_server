package com.geeklabs.footmark.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.footmark.domain.UserPreferences;
import com.geeklabs.footmark.repository.UserPreferencesRepository;
import com.geeklabs.footmark.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class UserPreferencesRepositoryImpl extends AbstractObjectifyCRUDRepository<UserPreferences> implements UserPreferencesRepository {

	@Autowired
	private Objectify objectify;

	public UserPreferencesRepositoryImpl() {
		super(UserPreferences.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}

	@Override
	public UserPreferences getUserPreferencesByUserId(Long userId) {
		 return objectify
				.load()
				.type(UserPreferences.class)
				.filter("user",objectify.load().type(com.geeklabs.footmark.domain.User.class).id(userId).now())
				.first()
				.now();
	}
}
