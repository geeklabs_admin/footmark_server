package com.geeklabs.footmark.repository.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.geeklabs.footmark.domain.Track;
import com.geeklabs.footmark.dto.AdvancedSearchDto;
import com.geeklabs.footmark.pagination.Pageable;
import com.geeklabs.footmark.repository.TrackRepository;
import com.geeklabs.footmark.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.cmd.LoadType;
import com.googlecode.objectify.cmd.Query;

@Repository
public class TrackRepositoryImpl extends AbstractObjectifyCRUDRepository<Track>
		implements TrackRepository {

	@Autowired
	private Objectify objectify;

	public TrackRepositoryImpl() {
		super(Track.class);
	}

	@Override
	protected Objectify getObjectify() {
		return objectify;
	}

	@Override
	public Track editTracksByTrackId(long trackId) {
		LoadType<Track> trackType = objectify.load().type(Track.class);
		Query<Track> query = trackType.filter("id", trackId);
		Track track = query.first().now();
		return track;
	}

	@Override
	public List<Track> getTracksDefaultTracks(long userId, Pageable pageable) {
		return objectify
				.load()
				.type(Track.class)
				.filter("user",objectify.load().type(com.geeklabs.footmark.domain.User.class).id(userId).now())
				.filter("content", null)
				.filter("isFavorite", false)
				.limit(Pageable.LIMIT)
				.offset(pageable.getOffset()).order("-trackTime").list();
	}
	
	@Override
	public List<Track> getUserOwnTracks(long userId, Pageable pageable) {
		return objectify
				.load()
				.type(Track.class)
				.filter("user",objectify.load().type(com.geeklabs.footmark.domain.User.class).id(userId).now())
				.filter("content !=", null)
				.limit(Pageable.LIMIT)
				.offset(pageable.getOffset()).order("content").order("-trackTime").list();
	}

	@Override
	public List<Track> getTracksByDate(long userId, AdvancedSearchDto searchDto,
			Pageable pageable) {
		String fromDat = "2013-11-25 12:34:54";
		String toDat = "2013-11-27 12:34:54";
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
		
		
		  String from = format.format(fromDat);
		  String to = format.format(toDat);
		  Date fromDate = null;
		  Date toDate = null;
		  try {
			 fromDate = format.parse(from);
			 toDate = format.parse(to);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return objectify.load().type(Track.class)
						.filter("user", objectify.load().type(com.geeklabs.footmark.domain.User.class).id(userId).now())
						.filter("tracktime >= ", fromDate)
						.filter("tracktime <= ", toDate)
						.limit(Pageable.LIMIT)
						.offset(pageable.getOffset())
						.order("-trackTime")
						.list();
	}

	@Override
	public List<Track> getSearchResults(long userId, Pageable pageable,
			String searchQuery) {
		if (searchQuery.startsWith("content")) {
			
			String[] split = searchQuery.split(":");
			if(split.length >= 2){
				String query = split[1];
				return objectify
					.load()
					.type(Track.class)
					.filter("user",objectify.load().type(com.geeklabs.footmark.domain.User.class).id(userId).now())
					.filter("content <= ", query)
					.filter("content >=", query+ "\uFFFD")
					.limit(Pageable.LIMIT)
					.offset(pageable.getOffset()).order("content").order("-trackTime").list();
			}
			
		}else if (searchQuery.startsWith("address")) {
			String[] split = searchQuery.split(":");
			if(split.length >= 2){
				String query = split[1];
				return objectify
						.load()
						.type(Track.class)
						.filter("user",objectify.load().type(com.geeklabs.footmark.domain.User.class).id(userId).now())
						.filter("address <= ", query)
						.filter("address >= ", query+ "\uFFFD")
						.limit(Pageable.LIMIT)
						.offset(pageable.getOffset()).order("address").order("-trackTime").list();
			}
		}
		
		return new ArrayList<Track>();
	}
	
	@Override
	public List<Track> getFavoriteTracksByUserId(long userId, Pageable pageable) {
		
		return objectify
				.load()
				.type(Track.class)
				.filter("user",objectify.load().type(com.geeklabs.footmark.domain.User.class).id(userId).now())
				.filter("isFavorite", true)
				.limit(Pageable.LIMIT)
				.offset(pageable.getOffset()).order("isFavorite").order("-trackTime").list();
	}

	@Override
	public Track isTrackExist(Date trackTime, Long userId) {
		return 	objectify
				.load()
				.type(Track.class)
				.filter("user",objectify.load().type(com.geeklabs.footmark.domain.User.class).id(userId).now())
				.filter("trackTime", trackTime)
				.first()
				.now();
	}
}
