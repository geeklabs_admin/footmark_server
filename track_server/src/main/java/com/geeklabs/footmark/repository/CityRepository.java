package com.geeklabs.footmark.repository;

import com.geeklabs.footmark.domain.City;
import com.geeklabs.footmark.repository.objectify.ObjectifyCRUDRepository;

public interface CityRepository extends ObjectifyCRUDRepository<City>{

}
