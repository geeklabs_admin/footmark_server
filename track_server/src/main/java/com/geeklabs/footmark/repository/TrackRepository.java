package com.geeklabs.footmark.repository;

import java.util.Date;
import java.util.List;

import com.geeklabs.footmark.domain.Track;
import com.geeklabs.footmark.dto.AdvancedSearchDto;
import com.geeklabs.footmark.pagination.Pageable;
import com.geeklabs.footmark.repository.objectify.ObjectifyCRUDRepository;

public interface TrackRepository extends ObjectifyCRUDRepository<Track> {
	Track editTracksByTrackId(long trackId);
	List<Track> getTracksDefaultTracks(long userId, Pageable pageable);
	List<Track> getTracksByDate(long userId, AdvancedSearchDto searchDto,Pageable pageable);
	List<Track> getUserOwnTracks(long userId, Pageable pageable);
	List<Track> getSearchResults(long userId, Pageable pageable, String searchQuery);
	List<Track> getFavoriteTracksByUserId(long userId, Pageable pageable);
	Track isTrackExist(Date trackTime, Long userId);
}
