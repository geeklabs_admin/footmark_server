package com.geeklabs.footmark.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;

import com.geeklabs.footmark.domain.State;
import com.geeklabs.footmark.repository.StateRepository;
import com.geeklabs.footmark.repository.objectify.AbstractObjectifyCRUDRepository;
import com.googlecode.objectify.Objectify;

public class StateRepositoryImpl extends AbstractObjectifyCRUDRepository<State> implements StateRepository {

	@Autowired
	private Objectify objectify;

	public StateRepositoryImpl() {
		super(State.class);
	}
	
	@Override
	protected Objectify getObjectify() {
		return objectify;
	}
}
