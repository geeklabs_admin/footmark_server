package com.geeklabs.footmark.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.geeklabs.footmark.response.json.ResponseStatus;
import com.geeklabs.footmark.service.UserService;

public class UserInterceptor implements HandlerInterceptor {

	private UserService userService;

	@Override
	public void afterCompletion(HttpServletRequest arg0,
			HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
	}

	@Override
	
	public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1,
			Object arg2, ModelAndView arg3) throws Exception {
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
			 Object arg2) throws Exception {
		long userId = 0;
		try {
			 String requestURI = request.getRequestURI();
			if(requestURI.contains("signin")) {
				return true;
			}
			
			userId = Long.parseLong(request.getParameter("userId"));
		} catch (NullPointerException | NumberFormatException e) {
			return false;
		}
		
		String uuid = request.getParameter("userUid");
		boolean isValidUuid = userService.validateUUID(uuid, userId);
		if (!isValidUuid) {
			ResponseStatus responseStatus = new ResponseStatus();
			responseStatus.setStatus("oldDeviceTracks");
			ObjectMapper mapper = new ObjectMapper();
			String status = mapper.writeValueAsString(responseStatus);
			response.getWriter().write(status);
		}
		return  isValidUuid;
	}
	
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
