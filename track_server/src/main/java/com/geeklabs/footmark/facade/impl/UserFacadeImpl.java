package com.geeklabs.footmark.facade.impl;

import java.util.Date;

import com.geeklabs.footmark.domain.User;
import com.geeklabs.footmark.dto.UserRegisterDto;
import com.geeklabs.footmark.facade.UserFacade;
import com.google.api.services.oauth2.model.Tokeninfo;

public class UserFacadeImpl implements UserFacade {

	@Override
	public User convertToUserDomain(Tokeninfo tokeninfo,
			UserRegisterDto userRegisterDto) {
		// Create user
		User user = new User();
		user.setEmail(tokeninfo.getEmail());
		user.setRegistrationDate(new Date());
		user.setUserUuid(userRegisterDto.getUserUuid());
		return user;
	}

}
