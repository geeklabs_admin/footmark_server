package com.geeklabs.footmark.facade.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.codehaus.jackson.map.ObjectMapper;
import org.dozer.DozerBeanMapper;
import org.springframework.web.client.RestTemplate;

import com.geeklabs.footmark.domain.City;
import com.geeklabs.footmark.domain.Country;
import com.geeklabs.footmark.domain.Document;
import com.geeklabs.footmark.domain.State;
import com.geeklabs.footmark.domain.Track;
import com.geeklabs.footmark.dto.CityDto;
import com.geeklabs.footmark.dto.CountryDto;
import com.geeklabs.footmark.dto.StateDto;
import com.geeklabs.footmark.dto.TrackDto;
import com.geeklabs.footmark.facade.TrackFacade;
import com.geeklabs.footmark.reversegeocoder.model.AddressComponent;
import com.geeklabs.footmark.reversegeocoder.model.GeoCodeResponse;
import com.geeklabs.footmark.reversegeocoder.model.Results;
import com.geeklabs.footmark.service.CityService;
import com.geeklabs.footmark.service.CountryService;
import com.geeklabs.footmark.service.DocumentService;
import com.geeklabs.footmark.service.StateService;

public class TrackFacadeImpl implements TrackFacade {

	private CountryService countryService;
	private StateService stateService;
	private CityService cityService;
	private DozerBeanMapper dozerBeanMapper;
	private DocumentService documentService;

	@Override
	public List<TrackDto> convertTrackDomainToTrackDto(List<Track> tracks) {
		List<TrackDto> trackDtoList = new ArrayList<TrackDto>();
		for (Track track : tracks) {
			
			// Get Image if exist 
			Document document = null;
			String imagePath = track.getCapturedImagePath();
			if (imagePath != null && !imagePath.isEmpty()) {
				document = documentService.getDocument(imagePath);
			}
			
			TrackDto trackDto = new TrackDto();
			dozerBeanMapper.map(track, trackDto);
			
			if (document != null) {
				trackDto.setCapturedImage(document.getContent());
			}
			
			trackDtoList.add(trackDto);
		}
		return trackDtoList;
	}

	@Override
	public Track convertTrackDtoToTrackDomain(TrackDto trackDto)
			throws ClientProtocolException, IOException {
		double latitude = trackDto.getLatitude();
		double longitude = trackDto.getLongitude();

		Track track = new Track();
		track.setTrackTime(trackDto.getTrackTime());
		track.setLatitude(latitude);
		track.setLongitude(longitude);
		track.setContent(trackDto.getContent());
		track.setTag(trackDto.getTag());
		track.setFavorite(trackDto.isFavorite());

		if (latitude > 0.0 && longitude > 0.0) {
			GeoCodeResponse addressFromLatLng = getAddressFromLatLng(latitude, longitude);
			if ("OK".equals(addressFromLatLng.getStatus())) {
				for (Results results : addressFromLatLng.getResults()) {
					for (AddressComponent addressComponent : results.getAddress_components()) {
						Collection<String> types = addressComponent.getTypes();
						if (types.isEmpty()) {
							continue;
						} else if (types.contains("route")) {
							track.setAddress(addressComponent.getLong_name());
						} else if (types.contains("sublocality")) {
							String address = track.getAddress();
							if (address != null) {
								track.setAddress(address.concat(",").concat(addressComponent.getLong_name()));
							}
						} else if (types.contains("locality")) {
							String address = track.getAddress();
							if (address != null) {
								track.setAddress(address.concat(",").concat(addressComponent.getLong_name()));
							}
						} else if (types.contains("administrative_area_level_2")) {
							CityDto cityDto = new CityDto();
							cityDto.setCode(addressComponent.getShort_name());
							cityDto.setName(addressComponent.getLong_name());
							City city = cityService.getCity(cityDto);
							track.setCity(City.key(city.getId()));
						} else if (types.contains("administrative_area_level_1")) {
							StateDto stateDto = new StateDto();
							stateDto.setCode(addressComponent.getShort_name());
							stateDto.setName(addressComponent.getLong_name());
							State state = stateService.getState(stateDto);
							track.setState(State.key(state.getId()));
						} else if (types.contains("country")) {
							CountryDto countryDto = new CountryDto();
							countryDto.setCode(addressComponent.getShort_name());
							countryDto.setName(addressComponent.getLong_name());
							Country country = countryService.getCountry(countryDto);
							track.setCountry(Country.key(country.getId()));
						} else if (types.contains("postal_code")) { // TODO
							// PostalCode postalCode = new PostalCode();
							// postalCode.setPostalCode(addressComponent.getLong_name());
						}
					}
					break;
				}
			}
		}
		return track;
	}

	private GeoCodeResponse getAddressFromLatLng(double lattitute,
			double longtitute) throws ClientProtocolException, IOException {
		String latitude = String.valueOf(lattitute);
		String longitude = String.valueOf(longtitute);
		String URL = "http://maps.google.com/maps/api/geocode/json?latlng="
				+ latitude + "," + longitude + "&sensor=true";

		RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		String forObject = restTemplate.getForObject(URL, String.class);
		System.out.println(forObject);
		GeoCodeResponse geoCodeResponse = mapper.readValue(forObject,
				GeoCodeResponse.class);
		return geoCodeResponse;
	}

	public void setDocumentService(DocumentService documentService) {
		this.documentService = documentService;
	}
	
	public void setCityService(CityService cityService) {
		this.cityService = cityService;
	}

	public void setCountryService(CountryService countryService) {
		this.countryService = countryService;
	}

	public void setStateService(StateService stateService) {
		this.stateService = stateService;
	}

	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}
}
