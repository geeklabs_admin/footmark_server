package com.geeklabs.footmark.facade;

import com.geeklabs.footmark.domain.User;
import com.geeklabs.footmark.dto.UserRegisterDto;
import com.google.api.services.oauth2.model.Tokeninfo;

public interface UserFacade {

	public User convertToUserDomain(Tokeninfo tokeninfo, UserRegisterDto userRegisterDto);
}
