package com.geeklabs.footmark.facade;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.geeklabs.footmark.domain.Track;
import com.geeklabs.footmark.dto.TrackDto;

public interface TrackFacade {

	public List<TrackDto> convertTrackDomainToTrackDto(List<Track> tracks);

	public Track convertTrackDtoToTrackDomain(TrackDto trackDtoList) throws ClientProtocolException, IOException;
}
