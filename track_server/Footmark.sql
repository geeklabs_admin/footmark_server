/*
SQLyog - Free MySQL GUI v5.01
Host - 5.0.21-community-nt : Database - footmark
*********************************************************************
Server version : 5.0.21-community-nt
*/


create database if not exists `footmark`;

USE `footmark`;

/*Table structure for table `city` */

DROP TABLE IF EXISTS `city`;

CREATE TABLE `city` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `city` */

/*Table structure for table `country` */

DROP TABLE IF EXISTS `country`;

CREATE TABLE `country` (
  `id` int(5) NOT NULL auto_increment,
  `code` varchar(2) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `country` */

/*Table structure for table `state` */

DROP TABLE IF EXISTS `state`;

CREATE TABLE `state` (
  `id` int(7) NOT NULL auto_increment,
  `code` varchar(50) NOT NULL,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `state` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL auto_increment,
  `email` varchar(250) NOT NULL,
  `fullName` varchar(50) default NULL,
  `registrationDate` date NOT NULL,
  `uuid` varchar(45) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `user` */

/*Table structure for table `userpreferences` */

DROP TABLE IF EXISTS `userpreferences`;

CREATE TABLE `userpreferences` (
  `id` bigint(20) NOT NULL auto_increment,
  `distance` float NOT NULL,
  `time` int(11) NOT NULL,
  `userid` bigint(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `userid_fk` (`userid`),
  CONSTRAINT `userpreferences_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `userpreferences` */

/*Table structure for table `track` */

DROP TABLE IF EXISTS `track`;

CREATE TABLE `track` (
  `id` bigint(20) NOT NULL auto_increment,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `trackTime` timestamp NOT NULL ,
  `address` varchar(350) NOT NULL,
  `userId` bigint(20) NOT NULL,
  `countryId` int(11) NOT NULL,
  `stateId` int(11) NOT NULL,
  `cityId` int(11) NOT NULL,
  `content` varchar(150),
  PRIMARY KEY  (`id`),
  KEY `FK_city` (`cityId`),
  KEY `FK_state` (`stateId`),
  KEY `FK_country` (`countryId`),
  KEY `FK_user` (`userId`),
  CONSTRAINT `track_ibfk_1` FOREIGN KEY (`cityId`) REFERENCES `city` (`id`),
  CONSTRAINT `track_ibfk_2` FOREIGN KEY (`stateId`) REFERENCES `state` (`id`),
  CONSTRAINT `track_ibfk_3` FOREIGN KEY (`countryId`) REFERENCES `country` (`id`),
  CONSTRAINT `track_ibfk_4` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `track` */
